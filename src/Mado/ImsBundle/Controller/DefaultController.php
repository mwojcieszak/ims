<?php

namespace Mado\ImsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('MadoImsBundle:Default:index.html.twig', array('name' => $name));
    }
}
