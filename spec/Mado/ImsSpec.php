<?php

namespace spec\Mado;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ImsSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Mado\Ims');
    }
}
